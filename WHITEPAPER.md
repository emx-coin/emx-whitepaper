Sander Bilo - EMX White Paper (draft)

# Introduction
The world bank states that global remittance is expected to increase to $642 billion in 2018. With average cost being 7.1% peaking up to 9.4% of the remitted amount when sending to Africa. This market is highly inefficient, slow, insecure, prone to human error and provides absolutely no transparency.

# EMX - Overview
EMX offers a digital stable coin aimed at global remittance it is build on proven technology and offers a modern easy to use user interface. 

The EMX ecosystem consists of 4 core areas: 

* __The Stellar Infrastructure:__ Stellar offers a proven, open-source, distributed, payments infrastructure, in which anybody can participate and contribute.
* __EMX Assets:__ The main asset is a USD pegged EMX coin, issued one on one to every USD in our custody. Next to the stable EMX currency several other currencies are being issued which are pegged to BTC, ETH and possibly others.
* __EMX Apps:__ The EMX ecosystem provides several apps to interact with the remittance network. A mobile first approach is used where IOS and Android are targeted. Next to this a web app is available to accommodate corporate accounts.
* __EMX Compliance:__ Transparency to government and endusers is one of the goals of this project. Part of the EMX toolset will be open-source software that allows anybody to validate the amount of EMX issued versus the amount of USD in our custody.

# EMX - Benefits
* EMX trough Stellar uses a unique way of creating consensus called the Federated Byzantine Agreement (FBA), unlike for example Bitcoin and Ethereum, the power consumption of this consensus protocol is very low and does not require any specialised equipment.
* The EMX coin is pegged to the USD, which guarantees the holder 1 USD for each EMX in their possession. There is transparency at all time about the amount of USD in our posession and the amount of EMX issued. 
* EMX can be exchanged against any asset available on the Stellar network, this implies remittance to or from any any asset can be realised. Exchanging assets is instant and transactional meaning minimal cost be achieved from exchanging trough intermediary parties.
* The Stellar network has a proven transaction speed of up to 10000 transactions per second, and a maximum time to settle of 2 to 5 seconds. These constrains are mainly caused by hardware limits and not necessarily limitations of the software.
* EMX utilises the Stellar networks open standards for securing information on transactions and accounts. In addition to this no information on our customers is stored.
* EMX allows to share payment requests using standard applications on the different mobile platforms. E.g. whatsapp, telegram, messenger and sms.
* EMX provides an alternative to Stellar anchors by adding a teller network to the mobile application providing access to local people selling or buying EMX for cash.
* EMX allows 3rd parties to white label the clients integrating them into their own systems. E.g. loyalty programs, sports pools or mobile shops.

# EMX - Technology stack
EMX is build on a state-of-the-art technology stack, providing api-services, libraries and open source clients.

* __Gitlab:__ Provides versioning and automated testing for the developers. It also allows to share code with other parties.
* __Kubernetes:__ Used to accelerate development and scalability of the platform.
* __Stellar:__ Open network which allows for issuing the EMX asset and provides all services required for remittance of digital assets.
* __React Native:__ Open-source Javascript library for developing mobile apps which run on Android and IOS.
* __React.js:__ Open-source Javascript library for developing web apps.
* __ProgreSQL:__ Open-source relational database for storing persistent structured data.

# Conclusion
Stellar offers a true free decentralised marketplace for assets, as one of the first companies to be recognised by official parties and banks EMX provides a safe point of entry into this market.
We believe EMX can provide the necessary ecosystem that people need to actually start benefiting from blockchain technology, not only offering reliable remittance but also an entry point for 3rd parties to start integration with their current systems and by doing so increasing efficiency and customer engagement.

# References
* https://stellar.org
* http://www.reactnative.com
* https://medium.com/a-stellar-journey/on-worldwide-consensus-359e9eb3e949
* https://www.postgresql.org
* https://www.americanbanker.com/news/how-barclays-aims-to-bring-a-billion-unbanked-into-the-fold
* http://www.worldbank.org/en/news/press-release/2018/04/23/record-high-remittances-to-low-and-middle-income-countries-in-2017


